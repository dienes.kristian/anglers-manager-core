package com.dienes.anglers.manager.core;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AnglersManagerCoreApplication {

	public static void main(String[] args) {

		SpringApplication.run(AnglersManagerCoreApplication.class, args);

	}
}
