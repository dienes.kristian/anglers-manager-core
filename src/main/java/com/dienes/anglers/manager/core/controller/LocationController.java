package com.dienes.anglers.manager.core.controller;

import com.dienes.anglers.manager.core.dao.impl.LocationDaoImpl;
import com.dienes.anglers.manager.core.model.Location;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;
import java.sql.SQLException;

/**
 * Created by kristian.dienes on 10/06/16.
 */

@RestController
@RequestMapping(
        path = "/location"
)
public class LocationController {

    @Inject
    private LocationDaoImpl locationDaoImpl;

    @RequestMapping(method = RequestMethod.POST, path = "/postLocation")
    public void addLocation() throws SQLException{
        locationDaoImpl.insertLocation();
    }

    @RequestMapping(method = RequestMethod.GET, path = "/{revirId}")
    public Location getLocationByRevirId(@PathVariable String revirId){
        return locationDaoImpl.getLocationByRevirId(revirId);
    }

}
