package com.dienes.anglers.manager.core.controller;

import com.dienes.anglers.manager.core.model.User;
import com.dienes.anglers.manager.core.dao.UserDao;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

/**
 * Created by kristian.dienes on 08/06/16.
 */

@RestController
@RequestMapping(
        path = "/user"
)
public class UserController {

    @Inject
    private UserDao userDao;

    @RequestMapping(method = RequestMethod.GET, path = "/{Id}")
    public User getUser(@PathVariable String Id){
        return userDao.getUserById(Id);
    }

    @RequestMapping(method = RequestMethod.POST)
    public void addUser(@RequestBody User user){
        userDao.save(user);
    }

    @RequestMapping(method = RequestMethod.GET, path = "/all")
    public List<Object> getAllUsers(){
        return StreamSupport.stream(userDao.findAll().spliterator(), false).collect(Collectors.toList());
    }

}

