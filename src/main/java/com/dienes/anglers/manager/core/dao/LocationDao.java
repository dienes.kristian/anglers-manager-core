package com.dienes.anglers.manager.core.dao;

import com.dienes.anglers.manager.core.model.Location;
import org.springframework.stereotype.Repository;

import java.sql.SQLException;

/**
 * Created by kristian.dienes on 10/06/16.
 */

@Repository
public interface LocationDao {

    Location getLocationByRevirId(String revirId);
    void insertLocation() throws SQLException;

}
