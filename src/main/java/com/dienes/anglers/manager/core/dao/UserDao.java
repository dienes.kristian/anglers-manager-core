package com.dienes.anglers.manager.core.dao;

import com.dienes.anglers.manager.core.model.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by kristian.dienes on 08/06/16.
 */
@Repository
public interface UserDao extends CrudRepository<User,String>{

    //TODO: as interface
    User getUserById(String id);
    List<User> findAll();

}
