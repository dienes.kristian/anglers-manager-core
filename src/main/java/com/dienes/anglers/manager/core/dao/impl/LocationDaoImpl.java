package com.dienes.anglers.manager.core.dao.impl;

import com.dienes.anglers.manager.core.config.H2DataSource;
import com.dienes.anglers.manager.core.dao.LocationDao;
import com.dienes.anglers.manager.core.model.Location;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by kristian.dienes on 10/06/16.
 */
@Repository
public class LocationDaoImpl implements LocationDao {

    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    @Autowired
    private H2DataSource h2DataSource;

    @Autowired
    public void setNamedParameterJdbcTemplate(NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
        this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
    }

    @Override
    public Location getLocationByRevirId(String revirId) {
        Map<String, String> params = new HashMap<String, String>();
        params.put("revirId", revirId);
        String sql = "SELECT * FROM locations WHERE revirId=:revirId";

        Location location = namedParameterJdbcTemplate.queryForObject(sql,params,new LocationMapper());

        return location;
    }

    @Override
    public void insertLocation() throws SQLException{
        h2DataSource.dataSource().getConnection().commit();
    }

    private static final class LocationMapper implements RowMapper<Location> {

        public Location mapRow(ResultSet rs, int rowNum) throws SQLException {
            Location location = new Location();
            location.setId(rs.getLong("id"));
            location.setCountry(rs.getString("country"));
            location.setLocationName(rs.getString("locationName"));
            location.setRevirId(rs.getString("revirId"));
            return location;
        }
    }

}
