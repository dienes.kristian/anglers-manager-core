package com.dienes.anglers.manager.core.model;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by kristian.dienes on 10/06/16.
 */
@Entity
public class Location implements Serializable {

    @Id
    @GeneratedValue
    private Long id;
    private String country;
    private String locationName;
    private String revirId;

    public String getLocationName() {
        return locationName;
    }

    public Long getId() {
        return id;
    }

    public String getCountry() {
        return country;
    }

    public void setLocationName(String locationName) {
        this.locationName = locationName;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRevirId() {
        return revirId;
    }

    public void setRevirId(String revirId) {
        this.revirId = revirId;
    }
}
