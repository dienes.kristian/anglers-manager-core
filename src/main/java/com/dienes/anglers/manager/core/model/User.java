package com.dienes.anglers.manager.core.model;

import javax.persistence.Id;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import java.io.Serializable;

/**
 * Created by kristian.dienes on 08/06/16.
 */
@Entity
public class User implements Serializable{

    @Id
    @GeneratedValue
    private String id;

    private String username;

    public void setId(String publicId){
        this.id = publicId;
    }

    public void setUsername(String username){
        this.username = username;
    }

    public String getUsername(){
        return this.username;
    }

    public String getId(){
        return this.id;
    }
}
