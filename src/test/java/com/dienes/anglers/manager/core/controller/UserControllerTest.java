package com.dienes.anglers.manager.core.controller;

import com.dienes.anglers.manager.core.model.User;
import com.dienes.anglers.manager.core.dao.UserDao;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.when;

/**
 * Created by kristian.dienes on 08/06/16.
 */

public class UserControllerTest {


    @InjectMocks
    private UserController userController;

    @Mock
    private UserDao userDao;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testGetById(){
        User expectedUser = new User();
        when(userDao.getUserById("1")).thenReturn(expectedUser);
    }

    @Test
    public void testGetAllUsers(){
        List<User> expectedList = new ArrayList<User>();
        when(userDao.findAll()).thenReturn(expectedList);
    }

}