package com.dienes.anglers.manager.core.dao.impl;

import com.dienes.anglers.manager.core.AnglersManagerCoreApplication;
import com.dienes.anglers.manager.core.model.Location;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabase;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;


import static org.junit.Assert.*;

/**
 * Created by kristian.dienes on 11/06/16.
 */

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations={"classpath:/beans.xml"})
@SpringApplicationConfiguration(classes = AnglersManagerCoreApplication.class)
public class LocationDaoTest {

    private EmbeddedDatabase db;

    @Autowired
    private LocationDaoImpl locationDaoImpl;

    @Before
    public void setUp() {
        db = new EmbeddedDatabaseBuilder()
                .setType(EmbeddedDatabaseType.H2)
                .addScript("create-db.sql")
                .addScript("insert-data.sql")
                .build();
    }

    @Test
    public void get_location_by_revir_id(){
        NamedParameterJdbcTemplate template = new NamedParameterJdbcTemplate(db);
        locationDaoImpl.setNamedParameterJdbcTemplate(template);

        Location location = locationDaoImpl.getLocationByRevirId("35310");
        assertNotNull(location);
        assertEquals("Levkuska",location.getLocationName());
    }

}